import http from 'http'

export default class HTTP {
  constructor (port = 8080) {
    http.createServer(this.onRequest).listen(port)
    return this
  }
  onRequest (req, res) {
    res.writeHead(200)
    res.end('hello')
  }
}
